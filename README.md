# 3rd rewrite of this bot

I decided to rewrite the automatic moderator of "a place to talk" in Python.
This might be a suprising development, considering how many times I've
complained about using the language for more than quick scripts, especially for
discord bots.

Truth is, I've not changed my mind. Python and other scripting languages still
only belong for setup scripts, personal scripts, and more. However, as time has
gone on I've lost interest in developing discord bots. As such, using Python
means I can still keep maintaining and improving this bot simply because it's
so much more productive than most compiled languages.

I should probably revive TimeyWimey with this also, it's not been updated for
way too long. But one step at a time.
