from discord.ext import commands
import asyncio
import discord
import os

# Modules
from .globals import CHANNEL_TMP, MESSAGE_TMP

from .cmds import Commands
from .freeze import Freeze
from .tmp import Temporary


def main():
    intents = discord.Intents.default()
    intents.members = True
    bot = commands.Bot(command_prefix="{}", intents=intents)

    cooldowns = set()

    @bot.listen()
    async def on_message(msg):
        if isinstance(msg.channel, discord.abc.PrivateChannel):
            if msg.author.bot or msg.author.id in cooldowns:
                return

            cooldowns.add(msg.author.id)

            for reaction in "🇳🇮🇨🇪🇹🇷🇾":
                await msg.add_reaction(reaction)

            await asyncio.sleep(10)
            cooldowns.discard(msg.author.id)

            return

        if bot.user in msg.mentions:
            await msg.add_reaction("::480337730153218055")


    bot.add_cog(Commands(bot))
    bot.add_cog(Freeze(bot))
    bot.add_cog(Temporary(bot))
    bot.run(os.environ["ABOTTOMOD_TOKEN"])
