import discord
from discord.ext.commands import Cog
from discord.ext import commands

from . import cmd_colour
from . import cmd_delall
from . import cmd_freeze
from . import cmd_poll
from . import cmd_result
from . import cmd_role
from . import cmd_toggle
from . import cmd_xkcd

class Commands(Cog):
    def __init__(self, bot):
        self.bot = bot
        bot.add_command(cmd_colour.colour)
        bot.add_command(cmd_delall.delall)
        bot.add_command(cmd_freeze.cmd_freeze)
        bot.add_command(cmd_poll.poll)
        bot.add_command(cmd_result.result)
        bot.add_command(cmd_role.role)
        bot.add_command(cmd_toggle.toggle)
        bot.add_command(cmd_xkcd.xkcd)

    @Cog.listener()
    async def on_command_error(self, ctx, error):
        await ctx.send(embed=(
            discord.Embed(
                title="Error",
                description=f"```{error}```",
                colour=0xff5555,
            )
            .set_footer(
                text="What the fuck have you done?",
            )
        ))
