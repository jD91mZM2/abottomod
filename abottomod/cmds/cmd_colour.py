import discord
from discord.ext import commands

from ..globals import ROLE_SEP_COLOUR
from .utils import is_human, find


@commands.command(aliases = ["color"])
@commands.check(is_human)
async def colour(ctx, colour=None):
    if colour is None:
        roles = ctx.author.roles
        roles.sort(reverse=True)
        colours = map(lambda role: role.colour.value, roles)
        colour = find(lambda colour: colour != 0, colours, default=0)
        await ctx.send(
            embed=discord.Embed(
                title="Your current colour is #{:X}".format(colour),
                description="Great choice!",
                colour=colour,
            ).set_footer(text="Delete your colour by setting it to 0")
        )
        return

    if colour.startswith('#'):
        colour = colour[1:]

    try:
        colour = int(colour, 16)
    except ValueError:
        await ctx.send("Usage: colour <hex>")
        return

    if colour < 0 or colour > 0xFFFFFF:
        await ctx.send("colour out of range 0 - 0xFFFFFF")
        return

    if colour == 0:
        role = find(
            lambda role: role.name == str(ctx.author.id),
            ctx.guild.roles
        )
        # Discord treats 0 as no colour, so deleting has the same effect
        await role.delete()

        await ctx.send(embed=discord.Embed(
            title="colour unset",
            description=(
                "Setting the colour to 0 makes it go away because "
                "discord sees it as an unspecified value. Therefore "
                "this bot also removes the internal role."
            )
        ))
        return

    colour = discord.Colour(colour)

    role = None
    try:
        role = find(
            lambda role: role.name == str(ctx.author.id),
            ctx.guild.roles,
            exception=True
        )
        await role.edit(colour=colour)
    except StopIteration:
        separator = find(
            lambda role: role.id == ROLE_SEP_COLOUR,
            ctx.guild.roles
        )
        role = await ctx.guild.create_role(
            name=ctx.author.id,
            colour=colour,
            permissions=discord.Permissions()
        )
        await role.edit(position=separator.position-1)

    if role not in ctx.author.roles:
        await ctx.author.add_roles(role)

    if ctx.command == "colour":
        await ctx.send("Thy colour has been set, sir!")
    else:
        await ctx.send("Color set!")
