import discord
from discord.ext import commands


@commands.command()
@commands.is_owner()
async def delall(ctx, since_msg: discord.Message):
    await ctx.channel.purge(limit=999, after=since_msg)
