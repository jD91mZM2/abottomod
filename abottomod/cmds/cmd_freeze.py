from discord.ext import commands
import discord

from .. import freeze


@commands.command(name = "freeze")
@commands.is_owner()
async def cmd_freeze(ctx, channel: discord.TextChannel):
    freeze.FROZEN_CHANNELS ^= set([channel.id])
    if channel.id in freeze.FROZEN_CHANNELS:
        await ctx.send("Channel name frozen")
    else:
        await ctx.send("Channel name changeable")
