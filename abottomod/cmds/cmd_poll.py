from discord.ext import commands
from typing import Optional

@commands.command()
async def poll(ctx, *, subject: Optional[str]):
    await ctx.message.add_reaction('👍')
    await ctx.message.add_reaction('👎')
