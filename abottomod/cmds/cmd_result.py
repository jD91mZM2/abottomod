from discord.ext import commands
import discord
import io
import matplotlib.pyplot as plt

from ..globals import ROLE_HUMAN
from .utils import find


@commands.command(aliases = ["results"])
async def result(ctx, message: discord.Message):
    try:
        pos_react = find(
            lambda react: react.emoji == '👍',
            message.reactions,
            exception=True
        )
        neg_react = find(
            lambda react: react.emoji == '👎',
            message.reactions,
            exception=True
        )
    except StopIteration:
        await ctx.send("No reactions")
        return

    pos = list(filter(
        lambda user: ROLE_HUMAN in map(
            lambda role: role.id,
            ctx.guild.get_member(user.id).roles
        ),
        await pos_react.users().flatten()
    ))
    neg = list(filter(
        lambda user: ROLE_HUMAN in map(
            lambda role: role.id,
            ctx.guild.get_member(user.id).roles
        ),
        await neg_react.users().flatten()
    ))

    if not pos and not neg:
        await ctx.send("No reactions")
        return

    plt.clf()
    plt.pie(
        [len(pos), len(neg)],
        labels=["Positive", "Negative"],
        colors=["green", "orange"],
        autopct="%1.2f%%"
    )

    stream = io.BytesIO()
    plt.savefig(stream, format="png")
    stream.seek(0)
    await ctx.send(file=discord.File(stream, "poll.png"))
