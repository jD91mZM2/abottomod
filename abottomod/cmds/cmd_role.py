from discord.ext import commands

from ..globals import ROLE_SEP_ASSIGNABLE
from .utils import find, is_human


@commands.command()
@commands.check(is_human)
async def role(ctx, mode, *rolenames):
    mode = mode.lower()

    separator = find(
        lambda role: role.id == ROLE_SEP_ASSIGNABLE,
        ctx.guild.roles
    )
    allowed = filter(
        lambda role: role < separator and not role.is_default(),
        ctx.guild.roles
    )

    if mode == "list":
        names = map(lambda role: role.name, sorted(allowed, reverse=True))
        await ctx.send(" ".join(names))
    elif mode == "add" or mode == "remove":
        if len(rolenames) == 0:
            await ctx.send(command_role.__doc__)
            return

        allowed = dict(map(
            lambda role: (role.name.lower(), role),
            allowed
        ))
        roles = set()
        for name in rolenames:
            name = name.lower()
            if name not in allowed:
                await ctx.send("unknown or disallowed role")
                return
            role = allowed[name]
            roles.add(role)

        if mode == "add":
            await ctx.author.add_roles(*list(roles))
        elif mode == "remove":
            await ctx.author.remove_roles(*list(roles))
        else:
            raise AssertionError("mode is not add or remove")

        await ctx.send("There you go!")
    else:
        await ctx.send(command_role.__doc__)
