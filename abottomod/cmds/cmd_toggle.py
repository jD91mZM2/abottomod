from discord.ext import commands

from ..globals import SETTINGS


@commands.command()
@commands.is_owner()
async def toggle(ctx, setting):
    setting = setting.lower()

    if setting in SETTINGS:
        SETTINGS[setting] = not SETTINGS[setting]
        await ctx.send(
            "Toggled, new value is `" + str(SETTINGS[setting]) + "`"
        )
    else:
        await ctx.send(
            "Unknown option. Options: " + ", ".join(SETTINGS.keys())
        )
