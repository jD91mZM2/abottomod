from discord.ext import commands
import requests


@commands.command()
async def xkcd(ctx, *, query):
    r = requests.get(
        "https://relevantxkcd.appspot.com/process",
        {"action": "xkcd", "query": query}
    )
    identifier = r.text.split()[2]
    await ctx.send("https://xkcd.com/" + identifier)
