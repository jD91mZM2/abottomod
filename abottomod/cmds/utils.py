import functools
import inspect
import math

from ..globals import ROLE_HUMAN, USER_AUTHOR


def is_human(ctx):
    ids = map(lambda role: role.id, ctx.author.roles)
    return ROLE_HUMAN in ids

def find(predicate, iterator, default=None, exception=False):
    """
    Returns the next item in the iterator that matches the given
    predicate, or the default value if no such item existed.
    Alternatively, setting exception to True will make the
    StopIteration error be raised.
    """
    try:
        return next(filter(predicate, iterator))
    except StopIteration as e:
        if exception:
            raise e
        return default
