from discord.ext.commands import Cog
import requests

from .globals import SETTINGS

FROZEN_CHANNELS = set()
SKIP_CHANNEL_UPDATE = 0

class Freeze(Cog):
    def __init__(self, bot):
        self.bot = bot
        self.skip_update = 0

    @Cog.listener()
    async def on_guild_channel_update(self, before, after):
        if self.skip_update > 0:
            # Bot just tried to send an event, assume this is it and ignore it.
            self.skip_update -= 1
            return

        if before.id in FROZEN_CHANNELS and before.name != after.name:
            self.skip_update += 1
            await after.edit(name=before.name)

        if SETTINGS["disallow_everyone"]:
            for key, value in after.overwrites.items():
                allow, deny = value.pair()
                if allow.mention_everyone:
                    self.skip_update += 1
                    value.mention_everyone = None
                    await after.set_permissions(key, overwrite=value)


    @Cog.listener()
    async def on_guild_emojis_update(self, guild, before, after):
        if not SETTINGS["undelete_emoji"]:
            return
        deleted = set(before) - set(after)

        for emoji in deleted:
            image = requests.get(emoji.url).content
            await emoji.guild.create_custom_emoji(name=emoji.name, image=image)
