CHANNEL_DELETED = 371956750229962752
CHANNEL_GENERAL = 275192340979646464
CHANNEL_TMP = 275193391254601728
CHANNEL_AUDIT = 748563450443202664
MESSAGE_TMP = 275557632062914560
ROLE_HUMAN = 275586691979411456
ROLE_SEP_ASSIGNABLE = 335435564629360641
ROLE_SEP_COLOUR = 531425557158690847
USER_AUTHOR = 172034577651597312

TMP_DURATION_SECS = 2 * 60

SETTINGS = {
    "undelete_emoji": True,
    "disallow_everyone": True
}
