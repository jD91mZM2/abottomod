from collections import namedtuple
from discord.ext import commands
from discord.ext.commands import Cog
from itertools import chain
from time import monotonic

from .globals import CHANNEL_TMP, TMP_DURATION_SECS, MESSAGE_TMP, CHANNEL_AUDIT
import asyncio

# TODO: Optimise this. It used to be fast, then I decided I wanted simpler
# code, now I might as well make it better again since it's cleaner

Ping = namedtuple("Ping", ["message", "count", "time"])
PINGS = dict()


class Temporary(Cog):
    def __init__(self, bot):
        self.bot = bot

    @Cog.listener()
    async def on_ready(self):
        # Clean #tmp
        channel = self.bot.get_channel(CHANNEL_TMP)
        await channel.purge(after=await channel.fetch_message(MESSAGE_TMP))

    @Cog.listener()
    async def on_message(self, msg):
        if msg.channel.id != CHANNEL_TMP:
            return

        async def clean_tmp():
            while True:
                await asyncio.sleep(TMP_DURATION_SECS)
                try:
                    await msg.delete()
                    break
                except discord.HTTPException as err:
                    if err.status == 404:
                        break
                    else:
                        # Ignore any network/permission error, although do wait
                        # before retrying
                        print("Error", err)

        # Register message to be cleaned in 2 minutes
        self.bot.loop.create_task(clean_tmp())

        # Alert mentions
        user_mentions = list(filter(
            lambda member: not member.bot and member.id != msg.author.id,
            msg.mentions,
        ))
        if len(user_mentions) > 0 or len(msg.role_mentions) > 0:
            general = msg.channel.guild.get_channel(CHANNEL_AUDIT)
            ping_str = ", ".join(map(
                lambda m: str(m),
                sorted(chain(msg.mentions, msg.role_mentions)),
            ))
            text = f"{msg.author.mention} just pinged {ping_str} in #tmp"
            if (
                    ping_str not in PINGS
                    or monotonic() - PINGS[ping_str].time >= 5*60
            ):
                PINGS[ping_str] = Ping(
                    message=await general.send(text),
                    count=1,
                    time=monotonic()
                )
            else:
                pings = PINGS[ping_str]
                await pings.message.edit(content=text + f" ({pings.count+1})")
                PINGS[ping_str] = Ping(
                    message=pings.message,
                    count=pings.count + 1,
                    time=monotonic(),
                )
