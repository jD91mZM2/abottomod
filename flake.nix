{
  description = "Moderator of aplacetotalk discord server";

  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
    in rec {
      # `nix build`
      packages.abottomod = pkgs.poetry2nix.mkPoetryApplication {
        projectDir = ./.;
      };
      defaultPackage = packages.abottomod;

      # `nix develop`
      devShell = pkgs.mkShell {
        LD_LIBRARY_PATH = "${pkgs.stdenv.cc.cc.lib}/lib";

        buildInputs = nixpkgs.lib.singleton (pkgs.poetry2nix.mkPoetryEnv {
          projectDir = ./.;
        });
        nativeBuildInputs = with pkgs; [ poetry ];
      };

      # `nix run`
      apps.abottomod = utils.lib.mkApp {
        drv = packages.abottomod;
      };
      defaultApp = apps.abottomod;
    });
}
